"use strict";

const app = document.getElementById("app");
const watch = document.getElementById("time");
const TIME = 18;
const dealers = [
  {
    id: 1,
    time: "30:00 minutos",
    status: "Disponible",
  },
  {
    id: 2,
    time: "30:00 minutos",
    status: "Disponible",
  },
  {
    id: 3,
    time: "30:00 minutos",
    status: "Disponible",
  },
  {
    id: 4,
    time: "30:00 minutos",
    status: "Disponible",
  },
  {
    id: 5,
    time: "30:00 minutos",
    status: "Disponible",
  },
  {
    id: 6,
    time: "30:00 minutos",
    status: "Disponible",
  },
  {
    id: 7,
    time: "30:00 minutos",
    status: "Disponible",
  },
  {
    id: 8,
    time: "30:00 minutos",
    status: "Disponible",
  },
];

//Creando el reloj para verificar el tiempo
setInterval(() => {
  const date = new Date();

  watch.innerHTML = `Son las ${
    date.getHours() % 12
  }:${date.getMinutes()}:${date.getSeconds()} ${
    date.getHours() < 12 ? "AM" : "PM"
  }`;
}, 1000);

//Creando un foreach para mostrar todos los datos
dealers.forEach((dealer) => {
  const template = `
  <div id="dealerContainer${dealer.id}" class="riders-items">
    <div class="item">
      <h3>${dealer.time}</h3>
    </div>

    <div class="item">
      <p>#${dealer.id}</p>
    </div>
    
    <div class="item">
      <button class="btn active" 
        dealer-id="${dealer.id}"
        onclick="handleClick(event)"
        >${dealer.status}</button>
    </div>
  </div>
`;
  app.innerHTML += template;
});

// eslint-disable-next-line no-unused-vars
function handleClick(event) {
  let dealerId = event.target.attributes.getNamedItem("dealer-id").value;
  console.log("handleClick", dealerId);

  if (!event.target.parentNode.parentNode.classList.contains("disabled")) {
    event.target.parentNode.parentNode.classList.add("disabled");
    event.target.classList.remove("active");
    event.target.classList.add("disabled");
    event.target.innerHTML = "Ocupado";
    if (!event.target.disabled) {
      event.target.disabled = true;
      dealers.forEach(function (dealer) {
        if (dealer.id == dealerId) {
          dealer.timer = TIME;
        }
      });
    }
    console.log(`El boton ha sido desactivado`);
  }
}

setInterval(() => {
  dealers.forEach((dealer) => {
    if (dealer.timer != null) {
      let element = document.getElementById("dealerContainer" + dealer.id);
      console.log(`${dealer.id}: ${dealer.timer}`);

      let mins = Math.floor(dealer.timer / 60);
      let remSec = dealer.timer % 60;

      let h3 = element.querySelector("h3");
      h3.innerHTML = `Restante: 
      ${String(mins).padStart(2, "0")}:${String(remSec).padStart(
        2,
        "0"
      )} minutos`;

      if (dealer.timer > 0) {
        dealer.timer--;
      } else {
        window.Swal.fire({
          icon: "success",
          title: "Completado",
          text: "El repartidor ha vuelto",
        });
        dealer.timer = null;
        h3.innerHTML = dealer.time;

        let btn = element.querySelector(".btn");
        btn.disabled = false;
        btn.parentNode.parentNode.classList.remove("disabled");
        btn.classList.remove("disabled");
        btn.innerHTML = "Disponible";
        dealers[dealer.id].timer = null;

        console.log(`El boton ha sido activado`);
      }
    }
  });
}, 1000);
